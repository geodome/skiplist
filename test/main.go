package main 

import (
	"fmt"
	"bitbucket.org/geodome/skiplist"
	"bitbucket.org/geodome/skiplist/debug"
)

func main() {
	layers := 4
	t := skiplist.NewSkipList(layers)
	for i:=11; i<20; i++ {
		t.Insert(i,i)
	}
	for i:=10; i>-1; i-- {
		t.Insert(i,i)
	}

	debug.Display(t)
	t.Delete(0)
	t.Delete(1)
	t.Delete(2)
	fmt.Println("Deleted", 0, 1, 2)
	debug.Display(t)
	t.Insert(2,2)
	t.Insert(1,1)
	t.Insert(3,3)
	t.Insert(0,0)
	debug.Display(t)
}

