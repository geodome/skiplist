package debug

import (
	"fmt"
	"bitbucket.org/geodome/skiplist"
)

func Display(s *skiplist.SkipList)  {
	layers := make([]string, s.Layers)
	keys := make([]int, 0)
	current := s.Head[0]
	for current != nil {
		keys = append(keys, current.Key)
		for i:=0; i<s.Layers; i++ {
			if i < len(current.Layers) {
				layers[i] = layers[i] + "--*"
			} else {
				layers[i] = layers[i] + "---"
			}
		}
		current, _ = current.GetForward(0)
	}
	for i:=len(layers)-1; i>=0; i-- {
		fmt.Println(layers[i])
	}
	for i:=0; i<len(keys); i++ {
		fmt.Printf("%3d", keys[i])
	}
	fmt.Println("")
}