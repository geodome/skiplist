package skiplist

type Link struct {
	Backward *Node
	Forward *Node
}

func (l *Link) IsHead() bool {
	return l.Backward == nil
}

func (l *Link) isTail() bool {
	return l.Forward == nil 
}
