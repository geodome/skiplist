package skiplist

import (
	"errors"
	"fmt"
)

type SkipList struct {
	Layers int
	Length int 
	Head []*Node
}

func NewSkipList(l int) *SkipList {
	return &SkipList {
		Layers: l,
		Head: make([]*Node, l),
	}
}

func (s *SkipList) NewLayerTracker() ([]*Node, error) {
	layertracker := make([]*Node, s.Layers)
	if s.Head[0] == nil {
		return nil, errors.New("Empty Head")
	}
	for h:=0; h<s.Head[0].Height(); h++ {
		layertracker[h] = s.Head[0]
	}
	return layertracker, nil 
}

func (s *SkipList) Contain(key int) bool {
	found, _, _, _ := s.Traverse(key)
	return found
}

func (s *SkipList) Traverse(key int) (bool, *Node, []*Node, error) {
	var err error
	current := s.Head[0]
	found := false
	layertracker, _ := s.NewLayerTracker()
	if current == nil {
		// Head Node is empty
		return false, nil, nil, errors.New("Traversing an empty Skip List")
	}
	for !found {
		prev := current
		current, err = current.Traverse(key)
		if err != nil {
			// Traverse error
			return false, nil, nil, err 
		}
		found = current.Key == key 
		if prev == current {
			// Zero traversal, indicating max key < search key is found.
			break
		} else {
			// update the layertracker
			for h:=0; h<current.Height(); h++ {
				layertracker[h] = current
			}
		}
	}
	return found, current, layertracker, nil
}

func (s *SkipList) Get(key int) (interface{}, error) {
	found, current, _, _ := s.Traverse(key)
	if found {
		return current.Value, nil 
	} else {
	
	return nil, errors.New("Invalid Key")
	}
}

func (s *SkipList) Insert(key int, value interface{}) {
	if s.Length == 0 {
		n := NewNode(key, value, s.Layers)
		for h:=0; h<n.Height(); h++ {
			s.Head[h] = n
		}
		s.Length ++
	} else {
		if key < s.Head[0].Key {
			n := NewNode(key, value, s.Layers)
			for h:=0; h<n.Height(); h++ {
				if s.Head[h] != nil {
					n.SetForward(h, s.Head[h])
					s.Head[h].SetBackward(h, n)
				}
				s.Head[h] = n
			}
			s.Length ++ 
		} else {
			found, current, layertracker, _ := s.Traverse(key)
			if found {
				current.Value = value
			} else {
				n := NewNode(key, value, s.Layers)
				for h:=0; h<n.Height(); h++ {
					if layertracker[h] != nil {
						temp, _ := layertracker[h].GetForward(h)
						layertracker[h].SetForward(h, n)
						n.SetForward(h, temp)
						n.SetBackward(h, layertracker[h])
					}
				}
				s.Length ++
			}
		}
	}
}

func (s *SkipList) Delete(key int) error {
	if s.Length == 0 {
		return errors.New("Empty Skip List")
	}
	found, current, layertracker, _ := s.Traverse(key)
	fmt.Println(found, current.Key)
	if found {
		for h:=current.Height()-1; h>-1; h-- {
			b, _ := layertracker[h].GetBackward(h)
			f, _ := layertracker[h].GetForward(h)
			if b!=nil {
				b.SetForward(h, f)
			}
			if f!=nil {
				f.SetBackward(h, b)
			}
			if s.Head[h] == current {
				s.Head[h], _ = current.GetForward(h)
				s.Head[h].SetBackward(h, nil)
			}
		}
		s.Length --
		return nil 
	}
	return errors.New("Invalid Key")
}


