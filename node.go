package skiplist

import (
	"math/rand"
	"errors"
)

type Node struct {
	Key int
	Value interface{}
	Layers []Link
}

func NewNode(key int, value interface{}, layers int) *Node {
	n := Node{
		Key: key,
		Value: value,
		Layers: make([]Link, 0),
	}
	for h:=0; h<layers; h++ {
		if h == 0 {
			n.Layers = append(n.Layers, Link{})
		} else {
			chances := rand.Int() % 100
			if chances > 50 {
				n.Layers = append(n.Layers, Link{})
			} else {
				break
			}
		}
	}
	return &n
}

func (n *Node) Height() int {
	return len(n.Layers)
}

func (n *Node) SetForward(layer int, f *Node) bool {
	if layer < n.Height() {
		if f == nil || layer < f.Height() {
			n.Layers[layer].Forward = f
			return true
		}
	}
	return false
}

func (n *Node) GetForward(layer int) (*Node, error) {
	if layer < n.Height() {
		return n.Layers[layer].Forward, nil 
	}
	return nil, errors.New("No such layer")
} 

func (n *Node) SetBackward(layer int, b *Node) bool {
	if layer < n.Height() {
		if b == nil || layer < b.Height() { 
			n.Layers[layer].Backward = b
			return true
		}
	}
	return false
}

func (n *Node) GetBackward(layer int) (*Node, error) {
	if layer < n.Height() {
		return n.Layers[layer].Backward, nil 
	}
	return nil, errors.New("No such layer")
}

func (n *Node) Traverse(key int) (*Node, error) {
	var temp *Node
	if key < n.Key {
		return nil, errors.New("Search key lesser than Node Key")
	}
	if key == n.Key {
		return n, nil 
	}
	for h:=0; h<n.Height(); h++ {
		 next, _ := n.GetForward(h)
		 if next != nil {
			 if next.Key <= key {
				 if temp == nil {
					 temp = next 
				 } else {
					 if next.Key > temp.Key {
						 temp = next
					 }
				 }
			 }
		 }
	}
	if temp == nil {
		return n, nil 
	}
	return temp, nil
}

func (n *Node) PopLayer() {
	n.Layers = n.Layers[:n.Height()-1]
}